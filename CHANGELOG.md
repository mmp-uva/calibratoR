# CHANGELOG

## 0.1.0 [WIP]

* Predict data from models
* Tidy models into serializable data.frames
* Calibration pipelines for Multi-Cultivator LEDs

