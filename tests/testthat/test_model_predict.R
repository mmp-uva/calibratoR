context("Model Predictions")
library(testthat)

test_that("test predict_tidy_lm function", {
  # test with simple dataset
  df.data <- tibble(x = seq(0, 4))
  df.model <- tibble(
    variable = rep("x", 2),
    coefficient = c(1, 2),
    degree  = c(0, 1)
  )

  predict_tidy_lm(df.model, df.data) %>%
    expect_equal(seq(0, 4) * 2 + 1)

  # test with intercept

  df.data <- tibble(x = seq(0, 4))
  df.model <- tibble(
    variable = c("(Intercept)", "x"),
    coefficient = c(1, 2),
    degree  = c(0, 1)
  )

  predict_tidy_lm(df.model, df.data) %>%
    expect_equal(seq(0, 4) * 2 + 1)

  # test with missing variable in dataset
  df.model <- tibble(
    variable = rep("y", 2),
    coefficient = c(1, 2),
    degree  = c(0, 1)
  )

  expect_error(
    predict_tidy_lm(df.model, df.data)
  )
})

test_that("test predict_data with tidy_model", {

  # test with pre-made tidy model
  df.data <- tibble(x = seq(0, 4))
  df.model <- tibble(
    variable = rep("x", 2),
    coefficient = c(1, 2),
    degree  = c(0, 1)
  )

  df.predict <- predict_data(df.model, tibble(x=seq(4, 10)), predicted, .f = predict_tidy_lm)
  expect_true( "predicted" %in% colnames(df.predict) )
  expect_equal( df.predict %>% pull(predicted) %>% unname(), seq(4, 10) * 2 +1 )

  # compare lm model via tidy_model with direct method
  model <- lm(Petal.Width ~ Sepal.Width + Petal.Length, iris)
  df.model <- tidy_model(model)
  predicted.lm <- predict_data(model, iris)
  predicted.tidy <- predict_data(df.model, iris, .f = predict_tidy_lm)

  expect_equal(predicted.lm, predicted.tidy)

})

test_that("test predict_data function", {

  # test without y column name
  df.data <- tibble(x = seq(0, 4)) %>%
    mutate(y = x * 2 + 1)
  model <- lm( y ~ x, df.data)

  predicted <- predict_data( model, tibble(x = seq(4, 10)) )
  expect_equal( predicted, seq(4, 10) * 2 +1 )

  # test with y column
  df.predict <- predict_data( model, tibble(x = seq(4, 10)), predicted )
  expect_true( "predicted" %in% colnames(df.predict) )
  expect_equal( df.predict %>% pull(predicted), seq(4, 10) * 2 +1 )

})

test_that("test predict_values function", {
  df.data <- tibble(x = seq(0, 4)) %>%
    mutate(y = x * 2 + 1)
  model <- lm( y ~ x, df.data)

  # test without y column
  predicted <- predict_values( model, x, seq(4, 10) )
  expect_equal( predicted, seq(4, 10) * 2 +1 )

  # test with y column
  df.predict <- predict_values( model, x, seq(4, 10), predicted )
  expect_true( "predicted" %in% colnames(df.predict) )
  expect_equal( df.predict %>% pull(predicted), seq(4, 10) * 2 +1 )

})


test_that("test predict_range function", {

  df.data <- tibble(x = seq(0, 4)) %>%
    mutate(y = x * 2 + 1)
  model <- lm( y ~ x, df.data)

  # test without y column
  predicted <- predict_range( model, x, 4, 10, .interval = 1 )
  expect_equal( predicted, seq(4, 10) * 2 +1 )

  # test with y column
  df.predict <- predict_range( model, x, 4, 10, y=predicted, .interval = 1 )
  expect_true( "predicted" %in% colnames(df.predict) )
  expect_equal( df.predict %>% pull(predicted), seq(4, 10) * 2 +1 )

})
