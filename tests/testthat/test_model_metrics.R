context("Model Metrics")
library(testthat)

test_that("test calculate_error function", {
  df.data <- tibble(
    predicted = seq(1, 6),
    actual = seq(0, 5)
  ) %>%
    mutate(
      error = calculate_error(predicted, actual)
    )

  expect_equal(rep(-1, 6), df.data$error)
})

test_that("test calculate_percentage_error function", {
  df.data <- tibble(
    predicted = seq(1, 6),
    actual = seq(0, 5)
  ) %>%
    mutate(
      percentage_error = calculate_percentage_error(predicted, actual)
    )

  expect_equal(c(NA, -1 / seq(1, 5)), df.data$percentage_error)
})

test_that("test calculate_mse function", {

  df.data <- tibble(
    predicted = seq(1, 6),
    actual = seq(0, 5)
  )

  mse <- calculate_mse(df.data$predicted, df.data$actual)

  expect_equal(1, mse)
})

test_that("test calculate_mape function", {

  df.data <- tibble(
    predicted = seq(1, 6),
    actual = seq(0, 5)
  )

  mape <- calculate_mape(df.data$predicted, df.data$actual)

  expect_equal(mean(1 / seq(1,5)), mape)

})

