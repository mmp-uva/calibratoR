context("Finding files")
library(testthat)


test_that("test collect function", {
  expect_error(collect_files())
  expect_error(collect_files(path = "./data/raw/validations"))
  expect_equal(length(collect_files(path = "./data/raw/validations", pattern = "^(.*/)?([0-9]{8})_(mc[0-9])_light_measurements.csv$")), 8)
  expect_equal(length(collect_files(path = "./data/raw/validations", pattern = "oh_no")), 0)
  expect_equal(length(collect_files(path = ".", pattern = "^(.*/)?([0-9]{8})_(mc[0-9])_light_measurements.csv$")), 0)
  expect_equal(length(collect_files(path = "./data/raw/calibrations", pattern = "^(.*/)?([0-9]{8})_(mc[0-9])_light_calibration.csv$")), 2)
})
