# Contributing to MMPR development

The goal of this guide is to help you contribute to `calibratoR` as easily as possible.

## Issues

## Code Style

* **Use tidyverse coding style**. Please follow the [tidyverse coding style](https://style.tidyverse.org). Maintaining a consistent style across the whole code base makes it much easier to jump into the code.
* When you change functions, you'll also need to document them with roxygen. Make sure to re-run `devtools::document()` on the code before submitting.
* Before submitting changes to the repo make sure to run the unittests. 

## Disclaimer

This guide was directly inspired by the `tidyverse` contribution guide as found [here](https://github.com/tidyverse/ggplot2/blob/master/CONTRIBUTING.md).
