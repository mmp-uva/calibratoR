
#' Tidy a Model into a serializable data.frame
#'
#' @param model object of class "lm" or object that is compatible with coef()
#' @param from optional lower limit of the model
#' @param to optional upper limit of the model
#'
#' @return tibble with columns variable, degree, coefficient, from, to
#'
#' @importFrom rlang .data
#' @export
tidy_model <- function(model, from = as.numeric(NA), to = as.numeric(NA)) {

  variables <- names(stats::coef(model))
  beta <- unname(stats::coef(model))

  tibble::tibble(
    variable = variables,
    degree = 1,
    coefficient = beta,
    from = from,
    to = to
  ) %>%
    dplyr::mutate(
      # extract degrees from poly terms
      degree = ifelse(
        stringr::str_starts(.data$variable, "poly\\("),
        sub("poly\\(.*\\)([0-9])$", "\\1", .data$variable),
        .data$degree
      ),
      # extract variable name from poly terms
      variable = ifelse(
        stringr::str_starts(.data$variable, "poly\\("),
        sub("poly\\(([^,]*),.*\\)", "\\1", .data$variable),
        .data$variable
      )
    )
}
