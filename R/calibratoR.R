#' calibratoR: Package to help with calibration of all instruments at the MMP lab
#'
#'
#' @docType package
#' @name calibratoR
#' @description Package to help with calibration of all instruments at the MMP lab
#'
#' @importFrom magrittr %>%
#' @importFrom rlang !! :=
NULL

utils::globalVariables(".")
