
#' Calculate Error for vectors of predicted and actual values
#'
#' Error = actual - predicted
#'
#' @param predicted vector with predicted values
#' @param actual vector with the actual values
#'
#' @return vector with error values
#'
#' @export
calculate_error <- function(predicted, actual) {
  actual - predicted
}

#' Calculate Percentage Error for vectors of predicted and actual values
#'
#' Percentage Error = (actual - predicted) / actual
#'
#' @param predicted vector with predicted values
#' @param actual vector with the actual values
#'
#' @return vector with percentage error values
#'
#' @export
calculate_percentage_error <- function(predicted, actual) {
  # make sure to remove values for which actual is zero
  actual <- ifelse(actual == 0, NA, actual)

  calculate_error(predicted, actual) / actual
}

#' Calculate Mean Squared Error (MSE) for vectors of predicted and actual values
#'
#' MSE = mean( (actual - predicted)^2 )
#'
#' @param predicted vector with the predicted values
#' @param actual vector with the actual values
#'
#' @return the calculated mean squared error
#'
#' @export
calculate_mse <- function(predicted, actual) {
  mean( calculate_error(predicted, actual)^2, na.rm=TRUE )
}

#' Calculate Mean Absolute Percentage Error (MAPE)  for vectors of predicted and actual values
#'
#' MAPE = mean( abs((actual - predicted) / actual) )
#'
#' @param predicted vector with the predicted values
#' @param actual vector with the actual values
#'
#' @return the calculated mean absolute percentage error
#'
#' @export
calculate_mape <- function(predicted, actual) {
  mean( abs(calculate_percentage_error(predicted, actual)), na.rm=TRUE )
}

